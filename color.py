from tensorflow.python.keras.layers import Dense, Dropout, LSTM, Reshape,TimeDistributed
from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.models import Sequential
from keras.callbacks import Callback, ModelCheckpoint
from tensorflow.python.keras import preprocessing,layers
from matplotlib import pyplot as plt
from keras.utils import np_utils
import numpy as np
import pandas as pd
import os.path
import requests
import io

class WriteEpoch(Callback):
    def on_epoch_end(self, epoch, logs={}):
        f = open('epochs.txt', 'w')
        f.write(str(epoch+1))
        f.close()

class Color():
    def __init__(self):
        self.url = "https://raw.githubusercontent.com/Tony607/Keras-Colors/master/colors.csv"
        self.maxlen = 25
        self.model = None
        self.data = None

        self.x = None
        self.y = None
        self.history = None
        self.tokenizer = None
        self.count_classes = 0
        self.index_2_word = dict()

    def load_data(self):
        if os.path.isfile('color.csv'):
            self.data = pd.read_csv('color.csv')
        else:
            url_content = requests.get(self.url).content
            csv_file = open('color.csv', 'wb')
            csv_file.write(url_content)
            csv_file.close()
            self.data = pd.read_csv(io.StringIO(url_content.decode('utf-8')))

    def preprocessing(self):
        names = self.data["name"]

        # Creating a tokenizer
        tokenizer = Tokenizer(lower=True)

        # Building word indices
        tokenizer.fit_on_texts(names)

        # Tokenizing sentences
        tokenized  = tokenizer.texts_to_sequences(names)

        # Creating a reverse dictionary
        self.word_2_index = tokenizer.word_index

        self.index_2_word = dict(map(reversed, self.word_2_index.items()))

        padded_names = preprocessing.sequence.pad_sequences(tokenized, maxlen=self.maxlen)
        self.y = np_utils.to_categorical(padded_names)
        self.count_classes = self.y.shape[-1]

        x = np.column_stack([self.norm(self.data["red"]), self.norm(self.data["green"]), self.norm(self.data["blue"])])
        self.x = x.reshape(len(x), 1, 3)

    def create_model(self):
        self.model = Sequential()
        self.model.add(LSTM(256, return_sequences=True, input_shape=(1,3))) # (1,3) shape of x
        self.model.add(Dense(128, activation='relu'))
        self.model.add(TimeDistributed(Dense(self.count_classes,activation='sigmoid'))) # number of classes
        self.model.compile(loss='mean_squared_error' , optimizer='adam',metrics=['acc'])
        self.model.summary()

    def train_model(self,valSplit,epochs,batch_size):
        writeEpoch = WriteEpoch()
        self.history = self.model.fit(self.x, self.y,
                            epochs=epochs,
                            batch_size=batch_size,
                            validation_split=valSplit,
                            callbacks=[writeEpoch,
                            ModelCheckpoint('weights.h5',
                            verbose=2,
                            save_weights_only=True)])

        plt.plot(self.history.history['acc'])
        if valSplit > 0.0:
            plt.plot(self.history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.xticks(np.arange(0, epochs + 1, 20.0))
        plt.yticks(np.arange(self.history.history['acc'][0] - 0.05, self.history.history['acc'][-1] + 0.05, 0.02))
        plt.legend(['train'], loc='upper left')
        if valSplit > 0.0:
            plt.legend(['val'], loc='upper left')
        plt.savefig('acc.png')
        plt.clf()
        plt.plot(self.history.history['loss'])
        if valSplit > 0.0:
            plt.plot(self.history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.xticks(np.arange(0, epochs + 1, 20.0))
        plt.yticks(np.arange(self.history.history['loss'][0] - 0.05, self.history.history['loss'][-1] + 0.05, 0.1))
        plt.legend(['train'], loc='upper left')
        if valSplit > 0.0:
            plt.legend(['val'], loc='upper left')
        plt.savefig('loss.png')

        with open('acc_loss.txt', 'w') as f:
            f.write(str(self.history.history['acc']))
            f.write('#')
            f.write(str(self.history.history['loss']))

    def loadModelWeights(self, wightsFileName):
        try:
            print('Loading weights...')
            self.model.load_weights(wightsFileName)
            print('Weights loaded')
        except Exception as e:
            print('Error:')
            print(str(e))

    def predict(self,array_rgb):
        word_sequence = ""
        pred = self.model.predict(array_rgb)
        pred = np.asarray([np.round(value * 100) for value in pred])
        pred = pred.astype(int)
        pred = pred.reshape(self.count_classes)
        for index in pred:
           if self.index_2_word.get(index) != None:
                word_sequence = word_sequence + ' ' + self.index_2_word.get(index)
        print(word_sequence)


    def norm(self,value):
        # The RGB values are between 0 - 255
        # scale them to be between 0 - 1
        return np.float32(value / 255)





