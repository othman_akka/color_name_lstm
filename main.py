from color import Color
import numpy as np

color = Color()
def train():
    color.load_data()
    color.preprocessing()
    color.create_model()
    color.train_model(valSplit=.1,epochs=100,batch_size=64)

def predict():
    color.load_data()
    color.preprocessing()
    color.create_model()
    color.loadModelWeights('weights.h5')

    array_rgb = [174, 182, 87]
    array_rgb_normalized = [color.norm(value) for value in array_rgb]
    array_rgb = np.array([[array_rgb_normalized]])
    color.predict(array_rgb)

if __name__=='__main__':
    #train()
    predict()