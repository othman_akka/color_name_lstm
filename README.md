# Detect a color name with Deep Learning

We can predict a name of color from RGB value

## Getting Started

### Prerequisites

The core of this project is built using python with keras and tensorflow libraries.

### Installing

You must install the libraries written in requirements.txt

You may first create a virtual environment for the project.
```
python3 -m venv venv
```

#### github :
get project from github with this commend.
```bash
https://gitlab.com/othman_akka/color_name_lstm.git
```

Before installing libraries.
Then, install required libraries:

```
pip install –r requirements.txt
```

### Training and Test

After setting the paths correctly, run the file ```main.py``` from python IDLE, from CMD or google colab by running the following:

``` python main.py ```

